﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SapirLesson19_
{
    public partial class Form1 : Form
    {
        private String _userName;
        private String _pass;

        public Form1()
        {
            InitializeComponent();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {// if you click on the EXIT button
            Application.Exit();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {//if you click the LOGIN button
            string[] lines = System.IO.File.ReadAllLines(@"Users.txt"); //read from the users file
            _userName = textBox1.Text; //user's input
            _pass = textBox2.Text; //user's input
            Dictionary<string, string> map = new Dictionary<string, string>(); // a map that the key is the username and the value is the password
            int counter = 0;
            foreach (string str in lines)
            {//spliting every line by ',' to username and password
                String LoginUserName = str.Split(',')[0];
                String LoginPassword = str.Split(',')[1];
                map.Add(LoginUserName, LoginPassword);
            }

            foreach (KeyValuePair<string, string> pair in map)
            {
                if (pair.Key == _userName && pair.Value == _pass)
                {//if the username and the password that the user entered matches the list then:
                    System.Windows.Forms.MessageBox.Show("Greetings, " + _userName);
                    Form2 form2 = new Form2();
                    form2.EnteredName = _userName; // the value in the form2 is the username that entered
                    form2.frm = this;
                    this.Hide();
                    form2.Show();
                    counter++;
                }
            }
            if (counter == 0)
            {
                System.Windows.Forms.MessageBox.Show("The username is not in the Users list!");
            }
        }
    }
}
