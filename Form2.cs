﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SapirLesson19_
{
    public partial class Form2 : Form
    {
        String _EnteredName = String.Empty;
        Dictionary<string, string> events = new Dictionary<string, string>();
        Form1 _frm = new Form1();

        public Form2()
        {
            InitializeComponent();
        }
        public string EnteredName
        {
            get { return _EnteredName; }
            set { _EnteredName = value; }
        }
        public Form1 frm
        {
            get { return _frm; }
            set { _frm = value; }
        }
        
        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
            String path = _EnteredName + "BD.txt";
            if (File.Exists(path) == true) // if the user has file with dates
            {
                string[] lines = System.IO.File.ReadAllLines(path);
                foreach (string str in lines)
                {
                    //getting the day/month/year from the date and changing it from string to int that matches the .net
                    int day = Int32.Parse((str.Split(',')[1]).Split('/')[1]);
                    int month = Int32.Parse((str.Split(',')[1]).Split('/')[0]);
                    int year = Int32.Parse((str.Split(',')[1]).Split('/')[2]);
                    //creating new birthday
                    DateTime BD = new DateTime(year, month, day);
                 //   monthCalendar1.AddMonthlyBoldedDate(birthDay);
                    events.Add(str.Split(',')[0], str.Split(',')[1]);
                }

            }
            else
            { //if the user doesnt have a file with dates - create one
                FileStream userFile = File.Create(path);

            }
        }


        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            String selectedDate = monthCalendar1.SelectionRange.Start.ToShortDateString(); // getting the selected date in the calendar into string
            int counter = 0;
            foreach (KeyValuePair<string, string> pair in events)
            {
                int dayFromList = Int32.Parse(pair.Value.Split('/')[1]); //getting the day from the list
                int chosenDay = Int32.Parse(selectedDate.Split('/')[0]); //getting the day that the user selected
                int monthFromList = Int32.Parse(pair.Value.Split('/')[0]); //getting the month from the list
                int chosenMonth = Int32.Parse(selectedDate.Split('/')[1]); //getting the month that the user selected
                if ((dayFromList == chosenDay) && (monthFromList == chosenMonth)) //if the two dates are matching:
                {
                    label1.Text = "בתאריך שנבחר ";
                    label2.Text = pair.Key;
                    label3.Text = " חוגג יום הולדת !";
                    counter++;
                }
            }
            if(counter == 0)
            {
                label1.Text = "בתאריך שנבחר ";
                label2.Text = "אף אחד";
                label3.Text = " לא חוגג יום הולדת !";
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {//if you click the X
            frm.Close();
        }
    }
}
